$('#touch-menu').click(function () {
    $(this).stop(0).addClass('opened');
    $('#sticker-menu').addClass('opened');
});
$('#sticker-menu .close-menu,#sticker-overlay').click(function () {
    $('#touch-menu').stop(0).removeClass('opened');
    $('#sticker-menu').removeClass('opened');
});
$('#sticker-menu .main-menu .fa').click(function () {
    $(this).parent('li').children('ul').stop(0).slideToggle(300);
    $(this).stop(0).toggleClass('fa-angle-down').toggleClass('fa-angle-up');
    $(this).parent('li').children('.sub-full').stop(0).slideToggle(300);
});
// $('.main-menu li a').each(function () { if (this.href.trim() == window.location) $(this).parent('li').addClass('active'); });
// $('.sub li a').each(function () { if (this.href.trim() == window.location) { $(this).parents('ul.main-menu li').addClass('active'); } });

$(document).ready(function(){
  if ($('#sticker-container').length) {
      var scrollTrigger = $('.header .top-header').height(); // px
      
      fixmenu = function () {
        var scrollTop = $(window).scrollTop();
        if ($(window).width()>991){
            if (scrollTop > scrollTrigger){
              $('#sticker-container').addClass('fixed');
              $('.content').addClass('padding');
            } else {
              $('#sticker-container').removeClass('fixed');
              $('.content').removeClass('padding');
            }            
        }
        else{
            if (scrollTop > 0){
              $('.header').addClass('fixed');
              $('.content').addClass('padding');
            } else {
              $('.header').removeClass('fixed');
              $('.content').removeClass('padding');
            }   
        }
      };
      fixmenu();
      $(window).on('scroll', function () {
        fixmenu();
      });
    }
  });
$(document).ready(function() {
  $('.slider-banner .slider').lightSlider({
    adaptiveHeight:true,
    item:1,
    slideMargin:0,
    loop:true,
    pager: false, 
    easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    mode:'fade',
    auto:true,
    pause: 5000,
    speed:600,
  });
});

$('.slider-product .slider').lightSlider({
  item: 5, 
  slideMove: 1, 
  slideMargin: 0, 
  pager: false, 
  controls: true, 
  pauseOnHover: true, 
  pause:7000,
  speed:600,
  responsive:[
  {
    breakpoint: 992,
    settings: {
      item:4
    }
  }, {
    breakpoint: 768,
    settings: {
      item: 3
    }
  }, {
    breakpoint: 480,
    settings: {
      item: 2
    }
  }
  ]
});

$(document).ready(function(){
  if ($('.back').length) {
      var scrollTrigger = 0, // px
      backToTop = function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > scrollTrigger) {
          $('.back').addClass('show');
        } else {
          $('.back').removeClass('show');
        }
      };
      backToTop();
      $(window).on('scroll', function () {
        backToTop();
      });
      $('.back').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
          scrollTop: 0
        }, 700);
      });
    }
  });
  $(document).ready(function () {  
    if($(window).width() > 991){
      $('.slider-img .owl-stage').lightSlider({
        item: 4, 
        slideMove: 1, 
        slideMargin: 0, 
        pager: false, 
        vertical:true,
        verticalHeight:334,
        controls: true, 
        pauseOnHover: true, 
        pause:7000,
        speed:600
      });
    }
    else{
      $('.slider-img .owl-stage').lightSlider({
        item: 3, 
        slideMove: 1, 
        slideMargin: 10, 
        pager: false, 
        vertical:false,
        controls: true, 
        pauseOnHover: true, 
        pause:7000,
        speed:600,
      });
    }
});
$(document).ready(function () {  
        $("#zoom_03").elevateZoom({ gallery: 'gallery_01', cursor: 'pointer', galleryActiveClass: "active", imageCrossfade: true, });
        $("#zoom_03").bind("click", function (e) {
            var ez = $('.zoom_03').data('elevateZoom');
            ez.closeAll();
            $.fancybox(ez.getGalleryList());
            return false;
        });
});
$('.nav-menu-title').click(function () {
  $(this).next('.tbl').stop(0).slideToggle(300);
  $(this).children('i').toggleClass('fa-caret-down').toggleClass('fa-caret-up');
});

$('.item-hover').hover(function(){
  $(this).children('.item.item100').addClass('block');
},function () {
  $(this).children('.item.item100').removeClass('block');
});